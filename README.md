# Shelter Food Calculator

This project contains a function that calculates how much good to order for the next month based on the number and sizes of various dogs and how much they are expected to eat.

## Prerequisites

This project relies on NodeJS and NPM to run. 

Install NodeJS(12+) with NPM on your system.

When it is installed, from the command line, go in to the root directory of the project and run `npm install` to install dependencies.

## Running Tests

To run the tests, from the project root directory, run `npm test`

