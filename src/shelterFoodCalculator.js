export function foodNeededNextMonth( small, medium, large, leftover) {
    if ( small == null || medium == null || large == null || leftover == null ){
        throw new Error( "Values must be entered for Small, Medium, Large, and Leftover" );
    }
    if ( typeof small !== "number" || typeof medium !== "number" || typeof large !== "number" || typeof leftover !== "number" ){
        throw new Error( "All values must be numbers" );
    }
    if ( small + medium + large > 30 ) {
        throw new Error( "Too many dogs" );
    }
    if ( small < 0 || medium < 0 || large < 0 || leftover < 0 ) {
        throw new Error( "All values must be positive numbers");
    }
    const foodNeeded = ( small * 10 ) + ( medium * 20 ) + ( large * 30 );
    let orderTotal = ( foodNeeded - leftover ) * 1.2;

    if ( orderTotal < 0 ) {
        orderTotal = 0;
    }
    return Math.round( orderTotal * 10 ) / 10;
}