import  { foodNeededNextMonth } from "../src/shelterFoodCalculator"

describe( "foodNeededNextMonth", () => {

  it( "should return error when there are too many dogs", async () => {
    expect( () => {
      foodNeededNextMonth( 10, 10, 11, 12 );
    } ) .toThrow( "Too many dogs" );
  } )

  it( "should return error when all params not included", async () => {
    expect( () => {
      foodNeededNextMonth( 10, 10 );
    } ).toThrow( "Values must be entered for Small, Medium, Large, and Leftover" );
  } )

  it( "should return error when invalid value sent", async () => {
    expect( () => {
      foodNeededNextMonth( 10, 10, 'abc', 'seventeen' );
    } ).toThrow( "All values must be numbers" );
  } )

  it( "should return error when negative value sent", async () => {
    expect( () => {
      foodNeededNextMonth( 10, 10, 6, -16 );
    } ).toThrow( "All values must be positive numbers" );
  } )

  it( "should return zero when there is a surplus", async () => {
    expect( foodNeededNextMonth( 1, 0, 0, 13 ) ).toEqual( 0 );
  } )

  it( "should return correct amount needed", async () => {
    expect( foodNeededNextMonth( 5, 3, 7, 17 ) ).toEqual( 363.6 );
    expect( foodNeededNextMonth( 1, 0, 0, 9.2 ) ).toEqual( 1 );
  } )
} )